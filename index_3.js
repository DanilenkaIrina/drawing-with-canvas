var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

const drawRoof = () => {
    ctx.beginPath();
    ctx.fillStyle = '#704343';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 5;
    
    ctx.moveTo(400,100);
    ctx.lineTo(200,300);
    ctx.lineTo(600,300);
    ctx.closePath();
    ctx.stroke();
    ctx.fill();    
};

const drawWall = () => {
    ctx.beginPath();
    ctx.fillStyle = '#704343';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 5;

    ctx.moveTo(200,300);
    ctx.lineTo(200,600);
    ctx.lineTo(600,600);
    ctx.lineTo(600,300);
    ctx.closePath();

    ctx.stroke();
    ctx.fill();    
};

const drawСhimney = () => {
    ctx.beginPath();
    ctx.fillStyle = '#704343';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 5;

    ctx.moveTo(500,250);
    ctx.lineTo(500,140);
    ctx.lineTo(460,140);
    ctx.lineTo(460,250);
    ctx.stroke();
    ctx.fill();    

    ctx.beginPath();
    ctx.fillStyle = '#704343';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 5;
    ctx.ellipse(480,139,20,8,0,0,Math.PI*2);
    ctx.stroke();
    ctx.fill();    
};

const drawWindow = (a,b) => {
    ctx.beginPath();
    ctx.fillStyle = '#000';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 3;

    ctx.moveTo(a,b);
    ctx.lineTo(a,b+30);
    ctx.lineTo(a+45,b+30);
    ctx.lineTo(a+45,b);
    ctx.lineTo(a,b);
    
    ctx.stroke();
    ctx.fill();    
};

const drawDoor = () => {
    ctx.beginPath();
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 3;

    ctx.moveTo(250,600);
    ctx.lineTo(250,510);
    ctx.quadraticCurveTo(300,470,350,510);
    ctx.lineTo(350,600);
    ctx.moveTo(300,490);
    ctx.lineTo(300,600);
    ctx.moveTo(285,570);
    ctx.arc(281.5,568,5,0,Math.PI*2,false);
    ctx.moveTo(317,570);
    ctx.arc(314,568,5,0,Math.PI*2,false);

    ctx.stroke();
};

drawRoof();
drawWall();
drawСhimney();
drawWindow(250,350);
drawWindow(305,350);
drawWindow(250,390);
drawWindow(305,390);

drawWindow(450,350);
drawWindow(505,350);
drawWindow(450,390);
drawWindow(505,390);

drawWindow(450,470);
drawWindow(505,470);
drawWindow(450,510);
drawWindow(505,510);
drawDoor();