var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

const drawWheel = (a,b) => {
    ctx.beginPath();
    ctx.fillStyle = '#61d5f8';
    ctx.strokeStyle = '#0d5c74';
    ctx.lineWidth = 6;
    
    ctx.arc(a,b,75,0,Math.PI*2,false);
    ctx.stroke();
    ctx.fill();    
};

const drawBicycle = () => {
    ctx.beginPath();
    ctx.fillStyle = '#61d5f8';
    ctx.strokeStyle = '#0d5c74';
    ctx.lineWidth = 3;
    
    ctx.moveTo(140,400);
    ctx.lineTo(300,400);
    ctx.moveTo(140,400);
    ctx.lineTo(250,300);
    ctx.moveTo(300,400);
    ctx.lineTo(220,240);
    ctx.lineTo(200,240);
    ctx.moveTo(180,240);
    ctx.lineTo(260,240);
    ctx.moveTo(250,300);
    ctx.lineTo(470,300);
    ctx.moveTo(470,300);
    ctx.lineTo(300,400);
    ctx.moveTo(500,400); 
    ctx.lineTo(450,240); 
    ctx.lineTo(400,260);
    ctx.moveTo(450,240); 
    ctx.lineTo(480,200); 
    ctx.moveTo(320,400);
    ctx.arc(300,400,20,0,Math.PI*2,false);
    ctx.moveTo(285,390);
    ctx.lineTo(260,370);
    ctx.moveTo(318,410);
    ctx.lineTo(343,430); 
    ctx.stroke();
};
drawWheel(150,400);
drawWheel(500,400);
drawBicycle();