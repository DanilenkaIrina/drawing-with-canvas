var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

const drawHead = () => {
    ctx.beginPath();
    ctx.fillStyle = '#61d5f8';
    ctx.strokeStyle = '#0d5c74';
    ctx.lineWidth = 7;
    
    ctx.moveTo(105,150);
    ctx.quadraticCurveTo(177,110,250,150);
    ctx.quadraticCurveTo(305,200,250,250);
    ctx.quadraticCurveTo(177,300,105,250);
    ctx.quadraticCurveTo(55,200,105,150);
    
    ctx.stroke();
    ctx.fill();    
};

const drawHat = () => {
    ctx.beginPath();
    ctx.fillStyle = '#0c5063';
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 7;
    
    ctx.ellipse(180, 140, 100, 25, 0, 0, Math.PI*2);
    ctx.stroke();
    ctx.fill();
    
    ctx.beginPath();
    ctx.moveTo(140,60);
    ctx.lineTo(140,130);
    ctx.quadraticCurveTo(180,150,220,130);
    ctx.lineTo(220,60);
    ctx.stroke();
    ctx.fill();

    ctx.beginPath();
    ctx.ellipse(180,60,40,25,0,0,Math.PI*2);
    ctx.stroke();
    ctx.fill();    
};

const drawEyes = () => {
    ctx.strokeStyle = '#0d5c74';
    ctx.lineWidth = 3;
    ctx.moveTo(140,200);
    ctx.ellipse(125,200,15,9,0,0,Math.PI*2);
    ctx.moveTo(210,197);
    ctx.ellipse(195,197,15,9,0,0,Math.PI*2);
    ctx.stroke();

    ctx.beginPath();
    ctx.fillStyle = '#0d5c74';
    ctx.moveTo(120,200);
    ctx.ellipse(120,199.5,4,8,0,0,Math.PI*2);
    ctx.moveTo(190,197);
    ctx.ellipse(190,197,5,7,0,0,Math.PI*2);
    ctx.fill();    
};

const drawNoseAndMouth = () => {
    ctx.strokeStyle = '#0d5c74';
    ctx.lineWidth = 3;
    ctx.moveTo(160,197);
    ctx.lineTo(150,228);
    ctx.stroke();
    ctx.lineTo(165,228);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(175,250);
    ctx.ellipse(150,245,26,9,Math.PI / 10,0,Math.PI*2);
    ctx.stroke();
};

drawHead();
drawHat();
drawEyes();
drawNoseAndMouth();